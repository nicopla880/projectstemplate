import requests, json

def create_user(user, password):

    url = "http://54.237.198.158:5000/create_user"
    data = {
        'user': user,
        'password': password
    }

    r = requests.get(url, params = data)

    return r.json()

if __name__ == "__main__":

    us = input("Ingrese la usuario: ")
    pas = input("Ingrese la contraseña: ")

    print(create_user(us, pas))