from datetime import datetime

class Persona(object):
    def __init__(self, nombre, dni, fecha_nacimiento):
        self.nombre = nombre
        self.dni = dni
        self.fecha_nacimiento = fecha_nacimiento
    
    def edad(self):
        _now = datetime.now()
        import ipdb; ipdb.set_trace()
        _birth_date = datetime.strptime(self.fecha_nacimiento, 
                                        '%d/%m/%Y')
        edad = _now - _birth_date
        return edad.days / 365 

if __name__ == '__main__':
    persona1 = Persona('Nicolas', '38181833', '28/04/1194')
    print(persona1.edad())
